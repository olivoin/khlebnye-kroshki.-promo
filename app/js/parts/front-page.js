$('.third-block-slider').slick({
    centerMode: true,
    variableWidth: true,
    slidesToShow: 3,
    slidesToScroll: 1,
    arrows: false
});

//open-close modal
$('.need-bread').click(function(){
   $('#callback-modal').addClass('active');
});

$('.modal-close').click(function(){
    $('#callback-modal').removeClass('active');
});