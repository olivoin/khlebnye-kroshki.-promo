jQuery(document).ready(function($) {

    // global

	transformicons.add('.tcon');
	$('.tel, input[name="phone"]').mask('+7 (999) 999-9999');

	$('.empty-link').click(function (e) {
		e.preventDefault();
	});
    
    // main
    
    $(".anchor-animate").click(function (e) {
        e.preventDefault();
        var a = $("[data-anchor='" + $(this).attr("href").replace("#", "") + "']");
        $("html, body").animate({
            scrollTop: (a.offset().top - 300) + 'px'
        });
    });
    
    // image to svg
    
    jQuery('img.svg').each(function () {
        var $img = jQuery(this);
        var imgID = $img.attr('id');
        var imgClass = $img.attr('class');
        var imgURL = $img.attr('src');

        jQuery.get(imgURL, function (data) {
            // Get the SVG tag, ignore the rest
            var $svg = jQuery(data).find('svg');

            // Add replaced image's ID to the new SVG
            if (typeof imgID !== 'undefined') {
                $svg = $svg.attr('id', imgID);
            }
            // Add replaced image's classes to the new SVG
            if (typeof imgClass !== 'undefined') {
                $svg = $svg.attr('class', imgClass + ' replaced-svg');
            }

            // Remove any invalid XML tags as per http://validator.w3.org
            $svg = $svg.removeAttr('xmlns:a');

            // Replace image with new SVG
            $img.replaceWith($svg);

        }, 'xml');
    });
    
});